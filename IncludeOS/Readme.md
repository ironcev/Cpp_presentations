Slides for the Qt/C++ User Group Graz talk and discussion on the `#include <os>` Unikernel.

Interesting Links:

* [IncludeOS on GitHub](https://github.com/hioa-cs/IncludeOS)
  
* [Free O'Reilly booklet on Unikernels](http://www.oreilly.com/webops-perf/free/unikernels.csp)

* [Unikernels and the Future of Cloud Computing](http://cloudacademy.com/blog/unikernels-docker-containers-vms-microservices/)